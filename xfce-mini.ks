# xfce-mini.ks 
#
# Very basic configurations for the xfce-mini release

%include common/base.ks
%include common/desktop-gtk.ks
%include common/xfce.ks

part / --size 6500 --fstype ext4

%include common/release.ks

