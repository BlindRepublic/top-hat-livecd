# Info
name		= top-hat
Name		= Top\ Hat
iso_suffix	= ${version}.$(release)-live.iso
version		= 0
release 	= 0

# Formats
iso_FORM	= $(name)-%-$(iso_suffix)

# Directories
cache_DIR 	= cache

# Files
release_KS	:= $(wildcard *.ks)
common_KS	:= $(wildcard common/*.ks)
image_ISO 	:= $(patsubst %.ks, $(iso_FORM), $(release_KS))

default: all

all: directories $(image_ISO) 

directories: 
	mkdir -p $(cache_DIR)

$(iso_FORM): %.ks $(common_KS)
	if [ "$$EUID" -ne 0 ]; \
  		then echo "Please build as root"; \
		exit -1;  \
	fi
	rm -rf cache/top-hat*
	livecd-creator -c $< -f $(subst .iso,,$@) \
		--cache=$(cache_DIR) -v  \
		--title=$(Name) --product=$(Name) 

clean:
	rm *.iso

clean-cache:
	if [ "$$EUID" -ne 0 ]; \
		then echo "Please build as root"; \
		exit -1; \
	fi
	rm -r $(cache_DIR)

clean-all: clean clean-cache

