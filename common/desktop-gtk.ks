%include desktop.ks

%packages

# Back-ends
xdg-desktop-portal-gtk
gvfs*

# Configuration
top-hat-gtk-config 

# System
gnome-firmware
mate-user-admin
gparted
gufw

# Files
evince
file-roller
gvim

# Misc
pavucontrol
qt5ct

%end
