# xfce.ks
# 
# XFCE config file for Top Hat Linux
# Repos
repo --name=xcape-copr --baseurl=https://download.copr.fedorainfracloud.org/results/dawid/xcape/fedora-$releasever-$basearch/
%post --nochroot
sudo dnf copr enable -y --installroot=$INSTALL_ROOT dawid/xcape >> /dev/null
%end

%packages

# Desktop
@xfce-desktop
@xfce-extra-plugins

# Remove xscreensaver since XFCE's built-in app is gtk3
-xscreensaver-base
xfce4-screensaver

# Menu editor
alacarte

# Configuration
top-hat-xfce-config

# Admin
lightdm-gtk-greeter-settings

# Theming (specified in config package)
greybird-*
elementary-xfce-icon-theme

# Files
ristretto
mousepad

# Remove unnecessary theming # FIXME: Should find better way to avoid these
-arc*
-xfwm4-themes
-bluebird-gtk3-theme
-albatross-gtk3-theme

%end

%post

# create /etc/sysconfig/desktop (needed for installation)

cat > /etc/sysconfig/desktop <<EOF
PREFERRED=/usr/bin/startxfce4
DISPLAYMANAGER=/usr/sbin/lightdm
EOF

# Configure lightdm theming
echo "Configuring Lightdm"
GREETER_CONFIG=/etc/lightdm/lightdm-gtk-greeter.conf
sed -i "s/#theme-name=/theme-name=Greybird-dark/" $GREETER_CONFIG
sed -i "s/#icon-theme-name=/icon-theme-name=elementary-xfce-dark/" $GREETER_CONFIG
sed -i "s/background=\/usr\/share\/backgrounds\/default.png/background=\/usr\/share\/backgrounds\/fedora-workstation\/cherryblossom.jpg/" $GREETER_CONFIG
echo "user-background=false" >> $GREETER_CONFIG

cat >> /etc/rc.d/init.d/livesys << EOF

mkdir -p /home/liveuser/.config/xfce4

# TODO: disable screensaver locking (#674410)

# deactivate xfconf-migration (#683161)
rm -f /etc/xdg/autostart/xfconf-migration-4.6.desktop || :

# set up lightdm autologin
sed -i 's/^#autologin-user=.*/autologin-user=liveuser/' /etc/lightdm/lightdm.conf
sed -i 's/^#autologin-user-timeout=.*/autologin-user-timeout=0/' /etc/lightdm/lightdm.conf
#sed -i 's/^#show-language-selector=.*/show-language-selector=true/' /etc/lightdm/lightdm-gtk-greeter.conf

# set Xfce as default session, otherwise login will fail
sed -i 's/^#user-session=.*/user-session=xfce/' /etc/lightdm/lightdm.conf

EOF


%end
