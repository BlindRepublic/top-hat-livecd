# desktop.ks
#
# Desktop-agnostic packages for top-hat that aren't part of the base

%packages

# System
@networkmanager-submodules
dnfdragora
gnome-keyring
system-config-printer
system-config-language
htop

# Hardware
@printing

# Theming
fedora-workstation-backgrounds

# Internet 
firefox
thunderbird

# Media
@multimedia
libdvdcss
mpv

# Development
git
wget

# Editors
nano
vim

# Remove unnecessary and enterprise-ish RedHat stuff
-abrt*
-firewall-config

%end

%post

# Remove dnfdragora-updator application from menu
cat >> /usr/share/applications/org.mageia.dnfdragora-updater.desktop << EOF
NoDisplay=true
EOF

cat >> /etc/rc.d/init.d/livesys << EOF

# Create install icon on desktop
sed -i -e 's/NoDisplay=true/NoDisplay=false/' /usr/share/applications/liveinst.desktop
mkdir /home/liveuser/Desktop
cp /usr/share/applications/liveinst.desktop /home/liveuser/Desktop
chmod +x /home/liveuser/Desktop/liveinst.desktop

# Disable Dnfdragora
rm -f /etc/xdg/autostart/org.mageia.dnfdragora-updater.desktop

EOF

%end
