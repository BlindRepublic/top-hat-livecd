%post
cat >> /etc/rc.d/init.d/livesys << EOF

# Set permissions
echo "Setting user permissions and ownership"
chown -R liveuser:liveuser /home/liveuser
restorecon -R /home/liveuser

EOF
%end
%post --nochroot
echo "ISO Preparation finished"
%end
